package hello;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingController {


    @Autowired
    DataService dataService;

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers() {
        return dataService.getUsers();
    }

    @RequestMapping(value = "/create-post", method = RequestMethod.GET)
    @ResponseBody
    public String createPost(@RequestBody String json) {
        Gson gson = new Gson();
        gson.fromJson(json, Post.class);

        // Database connect and insert data
        return "Greeting GET Method";
    }

    @RequestMapping(value = "/create-user", method = RequestMethod.GET)
    @ResponseBody
    public String createUser(@RequestBody String json) {
        Gson gson = new Gson();
        gson.fromJson(json, User.class);

        // Database connect and insert data
        return "Greeting GET Method";
    }

    @RequestMapping(value = "/greeting", method = RequestMethod.POST)
    @ResponseBody
    public String greetingPost(@RequestParam(value="name", defaultValue="World") String name) {
        return "Greeting POST Method";
    }

    @RequestMapping(value = "/greeting", method = RequestMethod.PUT)
    @ResponseBody
    public String greetingPut(@RequestParam(value="name", defaultValue="World") String name) {
        return "Greeting PUT Method";
    }

    @RequestMapping(value = "/greeting", method = RequestMethod.DELETE)
    @ResponseBody
    public String greetingDelete(@RequestParam(value="name", defaultValue="World") String name) {
        return "Greeting DELETE Method";
    }
}
